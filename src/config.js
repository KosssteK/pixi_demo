/**
 * Created by Wojciech on 2017-06-12.
 */
requirejs.config({
    baseUrl: 'src',
    paths: {
        PIXI: '../lib/pixi',
        TINK: '../lib/tink',
        main: 'main',
        game: 'Game',
        settings: 'Settings',
        hero: 'Hero/Hero',
        map: 'Map/Map',
        logic: 'Logic/Logic',
        enemy: 'Enemy/EnemyFactory',
        enemyT1: 'Enemy/EnemyType1',
        enemyT2: 'Enemy/EnemyType2'
    }


});
require(['main']);