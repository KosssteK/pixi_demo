/**
 * Created by Wojciech on 2017-06-13.
 */
define(['settings', 'PIXI', 'hero', 'map', 'TINK', 'logic', 'enemy'], function (Settings, pixi, Hero, Map, TINK, Logic, EnemyFactory) {








    Settings.renderer = PIXI.autoDetectRenderer(Settings.width, Settings.heigth);
    Settings.stage = new PIXI.Container();
    document.body.appendChild(Settings.renderer.view);




    var tink = new Tink(PIXI, Settings.renderer.view);
    var pointer = tink.makePointer();
    var hero = new Hero();
    var map = new Map();
    var enemyFactory = new EnemyFactory();
    var enemyList = [];


    var randomEnemy = '';





    PIXI.loader
        .add('hero', 'assets/heroes/hero4.png')
        .add('sky', 'assets/Background/sky.png')
        .add('grass', 'assets/Background/grass.png')
        .add('grass1', 'assets/Background/grass1.png')
        .add('enemyT1', 'assets/Creatures/enemyT1.png')
        .add('enemyT2', 'assets/Creatures/enemyT2.png')
        .load(setup);


    function setup() {

        map.build();

        hero.texture = PIXI.loader.resources['hero'].texture;
        hero.sprite = new PIXI.Sprite(hero.texture);
        hero.sprite.x = Settings.width/2;
        hero.sprite.y = Settings.heigth/2;








        Settings.stage.addChild(hero.sprite);
        Settings.renderer.render(Settings.stage);

        var left = keyboard(37),
            up = keyboard(38),
            right = keyboard(39),
            down = keyboard(40);








            left.press = () =>
            {
                hero.vx = -hero.playerSpeed;
                hero.vy = 0;
                hero.sideWaySpace = 0;
            }
            left.release = () =>
            {
                if (!right.isDown && hero.vy === 0) {
                    hero.vx = 0;
                    hero.sideWaySpace = 2;
                }
            }

            up.press = () =>
            {
                if(hero.jumpAvailable(map)){
                    hero.inTheAir = 1;
                }

            }
            up.release = () =>
            {

            }

            right.press = () =>
            {
                hero.vx = hero.playerSpeed;
                hero.vy = 0;
                hero.sideWaySpace = 1;
            }
            right.release = () =>
            {
                if (!left.isDown && hero.vy === 0) {
                    hero.vx = 0;
                    hero.sideWaySpace = 2;
                }
            }

            down.press = () =>
            {

            }
            down.release = () =>
            {
                if (!up.isDown && hero.vx === 0) {

                }
            }
            pointer.tap = () => {

                if(((pointer.y/Settings.blockHeigth) <(Settings.heigth/Settings.blockHeigth)-1) && (pointer.y/Settings.blockHeigth) > 1 ){
                    Logic.changeBackgroundBlock(map,pointer, hero, enemyList);
                }


            }


        gameLoop();
    }
    //==================================================================================================================

    function gameLoop() {


        requestAnimationFrame(gameLoop);

        tink.update();

        hero.move(map);
        if(hero.inTheAir) {
            hero.jump(map);
        }
        //Logic.gravityForce(hero,map);
        hero.gravityForce(map);

        if(enemyList.length < 2 ){

            if(Math.floor(Math.random()*50) < 25){
                randomEnemy = 'enemyT1';
            }else{
                randomEnemy = 'enemyT2';
            }

            enemyList[enemyList.length] = enemyFactory.createEnemy({
                enemyType: randomEnemy,
                sprite: new PIXI.Sprite(PIXI.loader.resources[randomEnemy].texture)
            });

        }else{
            for(var i = 0; i< enemyList.length; i++){
                Logic.gravityForce(enemyList[i],map);
                Logic.move(enemyList[i],map);
            }

        }





        Settings.renderer.render(Settings.stage);

    }
    //==================================================================================================================
    function keyboard(keyCode) {

        var key = {};

        key.code = keyCode;
        key.isDown = false;
        key.isUp = true;
        key.press = undefined;
        key.release = undefined;
        key.downHandler = event =>
        {
            if (event.keyCode === key.code) {
                if (key.isUp && key.press) key.press();
                    key.isDown = true;
                    key.isUp = false;
            }
            event.preventDefault();
        }


        key.upHandler = event =>
        {
            if (event.keyCode === key.code) {
                if (key.isDown && key.release) key.release();
                key.isDown = false;
                key.isUp = true;
            }
            event.preventDefault();
        }



        window.addEventListener(
            "keydown", key.downHandler.bind(key), false
        );
        window.addEventListener(
            "keyup", key.upHandler.bind(key), false
        );
        return key;

    }
});

