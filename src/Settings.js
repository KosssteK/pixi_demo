/**
 * Created by Wojciech on 2017-06-13.
 */
define(['PIXI'], function () {


    function Settings() {


        function init() {

            var renderer = null;
            var stage = null;
            var heigth = 512;
            var width = 1024;
            var blockHeigth = 32;
            var blockWidth = 32;
            var playerHeigth = 20;
            var playerWidth = 20;
            var gravity = 0;



            return {
                renderer: renderer,
                stage: stage,
                heigth: heigth,
                width: width,
                blockHeigth: blockHeigth,
                blockWidth: blockWidth,
                playerHeigth: playerHeigth,
                playerWidth: playerWidth,
                gravity:gravity

            };
        }

        return (function () {
            if (!this.instance) {
                this.instance = init();
            }
            return this.instance;
        })();


    }

    return new Settings;

});

