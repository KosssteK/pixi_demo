/**
 * Created by Wojciech on 2017-06-20.
 */

define(['settings', 'PIXI'], function (settings) {



    function Logic() {



        this.changeBackgroundBlock = function(map, pointer , hero, enemy){

            var x = Math.floor(pointer.x / settings.blockWidth);
            var y = Math.floor(pointer.y / settings.blockHeigth);


            if(map.backgroundTable[y][x].value === 1){



                if(map.backgroundTable[y+1][x].value === 1) {
                    map.backgroundTable[y+1][x].background.texture = PIXI.loader.resources['grass'].texture;
                }

                map.backgroundTable[y][x].value = 0;
                map.backgroundTable[y][x].background.texture = PIXI.loader.resources['sky'].texture;

                settings.stage.addChild(map.backgroundTable[y][x].background);
                for(var i = 0; i< enemy.length; i++) {
                    settings.stage.addChild(enemy[i].sprite);
                }
                settings.stage.addChild(hero.sprite);
                settings.renderer.render(settings.stage);

            }else{

                if(map.backgroundTable[y-1][x].value === 1)
                {
                    map.backgroundTable[y][x].background.texture = PIXI.loader.resources['grass1'].texture;
                    map.backgroundTable[y][x].value = 1;
                }else{
                    map.backgroundTable[y][x].background.texture = PIXI.loader.resources['grass'].texture;
                    map.backgroundTable[y][x].value = 1;
                    settings.stage.addChild(map.backgroundTable[y+1][x].background);
                }

                if(map.backgroundTable[y+1][x].value === 1) {
                    map.backgroundTable[y + 1][x].background.texture = PIXI.loader.resources['grass1'].texture;
                    settings.stage.addChild(map.backgroundTable[y + 1][x].background);
                }



                settings.stage.addChild(map.backgroundTable[y][x].background);
                for(var i = 0; i< enemy.length; i++) {
                    settings.stage.addChild(enemy[i].sprite);
                }
                settings.stage.addChild(hero.sprite);
                settings.renderer.render(settings.stage);
            }

        };
        this.gravityForce = function(obj , map){
            var pointsOfContact = [(obj.sprite.x), (obj.sprite.x + obj.objWidth - 1)];
            var gravityReset = 0;


            for (var i = 0; i < pointsOfContact.length; i++) {
                gravityReset += pointOfContact(map, obj, pointsOfContact[i]);
            }
            if (gravityReset === pointsOfContact.length) {
                obj.sprite.y += obj.gravity;
                obj.gravity += 1;
            }

        };
        this.move = function (obj, map) {

            if(obj.state=== 0){
                obj.vx = obj.creatureSpeed;
                obj.sideWaySpace = 1;
            }else {
                obj.vx = -obj.creatureSpeed;
                obj.sideWaySpace = 0;
            }

            if (obj.sprite.x + obj.objWidth + obj.vx < settings.width) {
                if (obj.sprite.x + obj.vx >= 0) {

                    if (wallCollision(map, obj)) {
                        obj.sprite.x += obj.vx;
                    } else {
                        obj.state = 1 - obj.state;
                    }

                } else {
                    obj.sprite.x += obj.creatureSpeed;
                    obj.state = 0;
                }
            } else {
                obj.sprite.x -= obj.creatureSpeed;
                obj.state = 1;
            }
        };

    }

    function pointOfContact(map, hero, point) {
        var i = Math.floor((hero.sprite.y + hero.objHeigth - 1) / settings.blockHeigth);
        var k = Math.floor((hero.sprite.y + hero.objHeigth) / settings.blockHeigth);
        var x = Math.floor((hero.sprite.y + hero.objHeigth + hero.gravity) / settings.blockHeigth);
        if (x > (settings.heigth / hero.objHeigth)) {
            x = settings.heigth / hero.objHeigth;
        }

        var j = Math.floor(point / settings.blockWidth);


        if (map.backgroundTable[i][j].value === map.backgroundTable[k][j].value) {

            if (map.backgroundTable[i][j].value === map.backgroundTable[x][j].value) {
                return 1;
            } else {
                hero.sprite.y = ((x - 1) * settings.blockHeigth) + (settings.blockHeigth - hero.objHeigth) ;
                hero.gravity = 0;
                hero.inTheAir = 0;
            }
        } else {
            hero.inTheAir = 0;
            hero.gravity = 0;
        }
    }

    function wallCollision(map, hero) {


        if (hero.sideWaySpace !== 2) {

            var pointsOfContact = [];
            pointsOfContact[0] = [ //left side of hero
                {
                    x: Math.floor(hero.sprite.x / settings.blockWidth),
                    y: Math.floor(hero.sprite.y / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x - 1) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + hero.vx) / settings.blockWidth)

                },
                {
                    x: Math.floor(hero.sprite.x / settings.blockWidth),
                    y: Math.floor((hero.sprite.y + (hero.objHeigth / 2)) / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x - 1) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + hero.vx) / settings.blockWidth)
                },
                {
                    x: Math.floor(hero.sprite.x / settings.blockWidth),
                    y: Math.floor((hero.sprite.y + (hero.objHeigth - 1)) / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x - 1) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + hero.vx) / settings.blockWidth)
                }
            ];
            pointsOfContact[1] = [ //right side of hero
                {
                    x: Math.floor((hero.sprite.x + hero.objWidth - 1) / settings.blockWidth),
                    y: Math.floor(hero.sprite.y / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x + hero.objWidth) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + hero.objWidth - 1 + hero.vx) / settings.blockWidth)
                },
                {
                    x: Math.floor((hero.sprite.x + hero.objWidth - 1) / settings.blockWidth),
                    y: Math.floor((hero.sprite.y + (hero.objHeigth / 2)) / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x + hero.objWidth) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + hero.objWidth - 1 + hero.vx) / settings.blockWidth)
                },
                {
                    x: Math.floor((hero.sprite.x + hero.objWidth - 1) / settings.blockWidth),
                    y: Math.floor((hero.sprite.y + hero.objHeigth - 1) / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x + hero.objWidth) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + hero.objWidth - 1 + hero.vx) / settings.blockWidth)
                }
            ];


            var contactCounter = 0;

            for (var j = 0; j < pointsOfContact[hero.sideWaySpace].length; j++) {

                if (
                    //=================|                    X                  ||                   Y                   | ========================
                map.backgroundTable[pointsOfContact[hero.sideWaySpace][j].y][pointsOfContact[hero.sideWaySpace][j].x].value ===
                map.backgroundTable[pointsOfContact[hero.sideWaySpace][j].y][pointsOfContact[hero.sideWaySpace][j].closest].value
                ) {

                    if (
                        //=================|                    X                  ||                   Y                   | ========================
                    map.backgroundTable[pointsOfContact[hero.sideWaySpace][j].y][pointsOfContact[hero.sideWaySpace][j].x].value ===
                    map.backgroundTable[pointsOfContact[hero.sideWaySpace][j].y][pointsOfContact[hero.sideWaySpace][j].accClosest].value
                    ) {
                        contactCounter++;

                    } else {
                        return false;

                    }


                } else {
                    return false;
                }
            }

            if (contactCounter === pointsOfContact[hero.sideWaySpace].length) {
                return true;
            }


        } else {
            return false;
        }

    }

    return new Logic;


});

