/**
 * Created by Wojciech on 2017-06-23.
 */
define(['settings'], function(settings){

    function CharacterLogic(){
        this.jumpAvailable = function(map){
            var pointsOfContact = [(this.sprite.x), (this.sprite.x + settings.playerWidth - 1)];


            for(var i = 0; i < pointsOfContact.length;i++){
                if(onTheGround(map,this, pointsOfContact[i]) === true){
                    return true;
                }
            }

        };
        this.move = function (map) {

            if ((this.sprite.x + settings.playerWidth + this.vx < (settings.width)) && (this.sprite.x + this.vx >= 0)) {
                //TODO: funkcja kolizji ze scianami


                if (wallCollision(map, this)) {
                    //if (true) {
                    this.sprite.x += this.vx;
                }

            }
        };
        this.jump = function (map) {
            //TODO: Logika dla kolizji z sufitem-
            var pointsOfContact = [(this.sprite.x), (this.sprite.x + settings.playerWidth - 1)];
            var gravityReset = 0;

            if ((this.sprite.y + (this.jumpingPower + this.gravity)) > 0) {

                for (var i = 0; i < pointsOfContact.length; i++) {
                    gravityReset += ceilingContact(map, this, pointsOfContact[i]);
                }
                if (gravityReset === pointsOfContact.length) {
                    this.sprite.y += this.jumpingPower;
                }

            } else {
                this.sprite.y = 0;
                this.inTheAir = 0;
                this.gravity = 0;
            }

        };
        this.gravityForce = function (map) {

            var pointsOfContact = [(this.sprite.x), (this.sprite.x + settings.playerWidth - 1)];
            var gravityReset = 0;


            for (var i = 0; i < pointsOfContact.length; i++) {
                gravityReset += pointOfContact(map, this, pointsOfContact[i]);
            }
            if (gravityReset === pointsOfContact.length) {
                this.sprite.y += this.gravity;
                this.gravity += 1;
            }

        };


    }


    function pointOfContact(map, hero, point) {
        var i = Math.floor((hero.sprite.y + settings.playerHeigth - 1) / settings.blockHeigth);
        var k = Math.floor((hero.sprite.y + settings.playerHeigth) / settings.blockHeigth);
        var x = Math.floor((hero.sprite.y + settings.playerHeigth + hero.gravity) / settings.blockHeigth);
        if (x > (settings.heigth / settings.playerHeigth)) {
            x = settings.heigth / settings.playerHeigth;
        }

        var j = Math.floor(point / settings.blockWidth);


        if (map.backgroundTable[i][j].value === map.backgroundTable[k][j].value) {

            if (map.backgroundTable[i][j].value === map.backgroundTable[x][j].value) {
                return 1;
            } else {
                hero.sprite.y = ((x - 1) * settings.blockHeigth) + (settings.blockHeigth - settings.playerHeigth) ;
                hero.gravity = 0;
                hero.inTheAir = 0;
            }
        } else {
            hero.inTheAir = 0;
            hero.gravity = 0;
        }
    }

    function ceilingContact(map, hero, point) {

        if((hero.gravity + hero.jumpingPower) < 0) {


            var i = Math.floor((hero.sprite.y) / settings.blockHeigth);
            var k = Math.floor((hero.sprite.y - 1) / settings.blockHeigth);
            var x = Math.floor((hero.sprite.y + (hero.gravity + hero.jumpingPower)) / settings.blockHeigth);
            if (x > (settings.heigth / settings.blockHeigth)) {
                x = settings.heigth / settings.blockHeigth;
            }

            var j = Math.floor(point / settings.blockWidth);


            if (map.backgroundTable[i][j].value === map.backgroundTable[k][j].value) {
                if (map.backgroundTable[i][j].value === map.backgroundTable[x][j].value) {
                    return 1;
                } else {
                    hero.sprite.y = i * settings.blockHeigth;
                    hero.gravity = 0;
                    hero.inTheAir = 0;
                }
            } else {
                hero.inTheAir = 0;
                hero.gravity = 0;
            }
        }else
        {
            return true;
        }
    }

    function wallCollision(map, hero) {


        if (hero.sideWaySpace !== 2) {

            var pointsOfContact = [];
            pointsOfContact[0] = [ //left side of hero
                {
                    x: Math.floor(hero.sprite.x / settings.blockWidth),
                    y: Math.floor(hero.sprite.y / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x - 1) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + hero.vx) / settings.blockWidth)

                },
                {
                    x: Math.floor(hero.sprite.x / settings.blockWidth),
                    y: Math.floor((hero.sprite.y + (settings.playerHeigth / 2)) / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x - 1) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + hero.vx) / settings.blockWidth)
                },
                {
                    x: Math.floor(hero.sprite.x / settings.blockWidth),
                    y: Math.floor((hero.sprite.y + (settings.playerHeigth - 1)) / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x - 1) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + hero.vx) / settings.blockWidth)
                }
            ];
            pointsOfContact[1] = [ //right side of hero
                {
                    x: Math.floor((hero.sprite.x + settings.playerWidth - 1) / settings.blockWidth),
                    y: Math.floor(hero.sprite.y / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x + settings.playerWidth) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + settings.playerWidth - 1 + hero.vx) / settings.blockWidth)
                },
                {
                    x: Math.floor((hero.sprite.x + settings.playerWidth - 1) / settings.blockWidth),
                    y: Math.floor((hero.sprite.y + (settings.playerHeigth / 2)) / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x + settings.playerWidth) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + settings.playerWidth - 1 + hero.vx) / settings.blockWidth)
                },
                {
                    x: Math.floor((hero.sprite.x + settings.playerWidth - 1) / settings.blockWidth),
                    y: Math.floor((hero.sprite.y + settings.playerHeigth - 1) / settings.blockHeigth),
                    closest: Math.floor((hero.sprite.x + settings.playerWidth) / settings.blockWidth),
                    accClosest: Math.floor((hero.sprite.x + settings.playerWidth - 1 + hero.vx) / settings.blockWidth)
                }
            ];


            var contactCounter = 0;

            for (var j = 0; j < pointsOfContact[hero.sideWaySpace].length; j++) {

                if (
                    //=================|                    X                  ||                   Y                   | ========================
                map.backgroundTable[pointsOfContact[hero.sideWaySpace][j].y][pointsOfContact[hero.sideWaySpace][j].x].value ===
                map.backgroundTable[pointsOfContact[hero.sideWaySpace][j].y][pointsOfContact[hero.sideWaySpace][j].closest].value
                ) {
                    if (
                        //=================|                    X                  ||                   Y                   | ========================
                    map.backgroundTable[pointsOfContact[hero.sideWaySpace][j].y][pointsOfContact[hero.sideWaySpace][j].x].value ===
                    map.backgroundTable[pointsOfContact[hero.sideWaySpace][j].y][pointsOfContact[hero.sideWaySpace][j].accClosest].value
                    ) {
                        contactCounter++;

                    } else {
                        if(hero.sideWaySpace === 1){
                            hero.sprite.x = (pointsOfContact[hero.sideWaySpace][j].x * settings.blockWidth)+ (settings.blockWidth - settings.playerWidth);  //TODO: there might be something wrong :)
                        }else{
                            hero.sprite.x = (pointsOfContact[hero.sideWaySpace][j].x * settings.blockWidth);  //TODO: there might be something wrong :)
                        }
                        break;
                    }


                } else {
                    return false;
                }
            }

            if (contactCounter === pointsOfContact[hero.sideWaySpace].length) {
                return true;
            }


        } else {
            return false;
        }

    }

    function onTheGround(map, hero, point) {
        var i = Math.floor((hero.sprite.y + settings.playerHeigth - 1) / settings.blockHeigth);
        var k = Math.floor((hero.sprite.y + settings.playerHeigth) / settings.blockHeigth);

        var j = Math.floor(point / settings.blockWidth);

        if (map.backgroundTable[i][j].value === map.backgroundTable[k][j].value) {
            return false;
        } else {
            return true;
        }
    }




    return CharacterLogic;

});