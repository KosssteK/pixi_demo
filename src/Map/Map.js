/**
 * Created by Wojciech on 2017-06-13.
 */
define(['settings', 'hero'],function(settings){

    function Map(){



        this.backgroundTable = [];
        for(var i =0; i<settings.heigth/settings.blockHeigth; i++)
        {
            this.backgroundTable[i] = [];
            for(var j =0; j<settings.width/settings.blockWidth; j++)
            {
                this.backgroundTable[i][j] = 0;
            }
        }

    }

    Map.prototype.build = function(){
        for(var i =0; i<this.backgroundTable.length; i++){
            for(var j =0; j<this.backgroundTable[i].length; j++) {

            //building grass and sky
            //==========================================================================================================
                if(i === this.backgroundTable.length-1){
                    this.backgroundTable[i][j] = {background:new PIXI.Sprite(PIXI.loader.resources['grass'].texture), value:1};
                }else{
                    this.backgroundTable[i][j] = {background:new PIXI.Sprite(PIXI.loader.resources['sky'].texture), value:0};
                }



            //==========================================================================================================




                //console.log(this.backgroundTable[i][j]);
                //positioning grass and sky blocks
                this.backgroundTable[i][j].background.x =  j*settings.blockWidth;
                this.backgroundTable[i][j].background.y =  i*settings.blockHeigth;
                settings.stage.addChild(this.backgroundTable[i][j].background);
            }
        }

    };



    return Map;


});

/*


define(['settings', 'hero'],function(settings){

    function Map(){


        function initialization(){
            var backgroundTable = [];
            for(var i =0; i<settings.heigth/settings.blockHeigth; i++)
            {
                backgroundTable[i] = [];
                for(var j =0; j<settings.width/settings.blockWidth; j++)
                {
                    backgroundTable[i][j] = 0;
                }
            }

            var built = function(options){
                for(var i =0; i<backgroundTable.length; i++){
                    for(var j =0; j<backgroundTable[i].length; j++) {


                        //building grass and sky
                        //==========================================================================================================
                        if(i === backgroundTable.length-1){
                            backgroundTable[i][j] = {background:options.sprite1, value:1};
                        }else{
                            backgroundTable[i][j] = {background:options.sprite2, value:0};
                        }

                        //==========================================================================================================

                        //console.log(this.backgroundTable[i][j]);
                        //positioning grass and sky blocks
                        backgroundTable[i][j].background.x =  j*settings.blockWidth;
                        backgroundTable[i][j].background.y =  i*settings.blockHeigth;
                        settings.stage.addChild(backgroundTable[i][j].background);
                    }
                }

            };


            return {
                backgroundTable:backgroundTable,
                build: built
            };
        }
        return (function () {
            if (!this.mapInstance) {
                this.mapInstance = initialization();
            }
            return this.mapInstance;
        })();

    }


    return new Map;


});
 */