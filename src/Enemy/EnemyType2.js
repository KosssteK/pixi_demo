/**
 * Created by Wojciech on 2017-06-22.
 */

define(['settings'],function(settings){

    function EnemyType2(options){
        this.sprite = options.sprite ||null;
        this.creatureSpeed = 2;
        this.sideWaySpace = 0;
        this.objWidth = 32;
        this.objHeigth = 32;
        this.gravity = 0;
        this.vx = 0;
        this.state = 0;
        if(this.sprite !==null){
            this.sprite.x = Math.floor(Math.random()*50 + 4);
            this.sprite.y = Math.floor(Math.random()*50 + 4);

            settings.stage.addChild(this.sprite);
            settings.renderer.render(settings.stage);

        }


    }
    return EnemyType2;

});