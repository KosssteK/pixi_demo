/**
 * Created by Wojciech on 2017-06-22.
 */
define(['settings', 'enemyT1', 'enemyT2'], function (settings, EnemyT1, EnemyT2) {


    function EnemyFactory(){

    }
    EnemyFactory.prototype.instance = EnemyT1;
    EnemyFactory.prototype.createEnemy = function(options){

        switch(options.enemyType){
            case 'enemyT1':
                this.instance = EnemyT1;
                break;
            case 'enemyT2':
                this.instance = EnemyT2;
                break;

        }
        return new this.instance(options);
    };
    return EnemyFactory;

});