/**
 * Created by Wojciech on 2017-06-22.
 */


define(['settings', 'map'],function(settings){

    function EnemyType1(options){
        this.sprite = options.sprite ||null;
        this.creatureSpeed = 3;
        this.sideWaySpace = 0;
        this.objWidth = 32;
        this.objHeigth = 32;
        this.gravity = 0;
        this.vx = 0;
        this.state = 0;

        if(this.sprite !==null){
            this.sprite.x = Math.floor(Math.random()*200 + 4);
            this.sprite.y = Math.floor(Math.random()*200 + 4);

            settings.stage.addChild(this.sprite);
            settings.renderer.render(settings.stage);

        }


    }
    // EnemyType1.prototype.gravityForce = function (map) {
    //
    //     var pointsOfContact = [(this.sprite.x), (this.sprite.x + this.enemyWidth - 1)];
    //     var gravityReset = 0;
    //
    //
    //     for (var i = 0; i < pointsOfContact.length; i++) {
    //         gravityReset += pointOfContact(map, this, pointsOfContact[i]);
    //     }
    //     if (gravityReset === pointsOfContact.length) {
    //         this.sprite.y += this.gravity;
    //         this.gravity += 1;
    //     }
    //
    // };
    //
    // function pointOfContact(map, hero, point) {
    //     var i = Math.floor((hero.sprite.y + hero.enemyHeigth - 1) / settings.blockHeigth);
    //     var k = Math.floor((hero.sprite.y + hero.enemyHeigth) / settings.blockHeigth);
    //     var x = Math.floor((hero.sprite.y + hero.enemyHeigth + hero.gravity) / settings.blockHeigth);
    //     if (x > (settings.heigth / hero.enemyHeigth)) {
    //         x = settings.heigth / hero.enemyHeigth;
    //     }
    //
    //     var j = Math.floor(point / settings.blockWidth);
    //
    //
    //     if (map.backgroundTable[i][j].value === map.backgroundTable[k][j].value) {
    //
    //         if (map.backgroundTable[i][j].value === map.backgroundTable[x][j].value) {
    //             return 1;
    //         } else {
    //             hero.sprite.y = ((x - 1) * settings.blockHeigth) + (settings.blockHeigth - hero.enemyHeigth) ;
    //             hero.gravity = 0;
    //             hero.inTheAir = 0;
    //         }
    //     } else {
    //         hero.inTheAir = 0;
    //         hero.gravity = 0;
    //     }
    // }

    return EnemyType1;

});